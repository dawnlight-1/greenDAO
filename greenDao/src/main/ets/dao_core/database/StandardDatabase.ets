/*
 * Copyright (C) 2011-2016 Markus Junginger, greenrobot (http://greenrobot.org)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import dataRdb from '@ohos.data.rdb'
import {Database} from './Database'
import {DatabaseStatement} from './DatabaseStatement';
import {SQLiteStatement} from './SQLiteStatement';
import {StandardDatabaseStatement} from './StandardDatabaseStatement';
import {OnTableChangedListener} from '../dbflow/listener/OnTableChangedListener'
import {TableAction} from '../dbflow/listener/TableAction';

export class StandardDatabase implements Database {
  //@ts-ignore
  private delegate: dataRdb.RdbStore;
  protected myOnTableChangedListener=null;
  //@ts-ignore
  public constructor(delegate: dataRdb.RdbStore) {
    this.delegate = delegate;
  }
  // @ts-ignore
   async rawQuery(predicates: dataRdb.RdbPredicates, selectionArgs: string[]): Promise<any /*ResultSet*/> {

//     return this.delegate.query(predicates,selectionArgs);

     let that = this;
     // @ts-ignore
     return new Promise<any /*ResultSet*/>(resolve => {
       let listener=this.myOnTableChangedListener;
       that.delegate.query(predicates,selectionArgs).then((data)=>{
         resolve(data);
         if(listener!=null){
           listener.onTableChanged(data, undefined, TableAction.QUERY);
         }
       }).catch((err) => {
         if(listener!=null) {
           listener.onTableChanged(undefined, err, TableAction.QUERY);
         }
       })
     });
  }
  // @ts-ignore
  async Delete(predicates: dataRdb.RdbPredicates): Promise<any /*ResultSet*/> {

    let that = this;
    return new Promise<number>(resolve => {
      let listener=this.myOnTableChangedListener;
      that.delegate.delete(predicates).then((data)=>{
        if(listener!=null){
          listener.onTableChanged(data, undefined, TableAction.DELETE);
        }
        resolve(data);
      }).catch((err) => {
        if(listener!=null) {
          listener.onTableChanged(undefined, err, TableAction.DELETE);
        }
      })
    });
  }

  beginTransaction() {
  }

  endTransaction() {
  }

  inTransaction(): boolean {
    throw new Error("not support");
  }
  setTransactionSuccessful() {
  }

  execSQL(sql: string, bindArgs?: any[]) {
    this.delegate.executeSql(sql, bindArgs, function (err) {
      if (err) {
        console.info('StandardDatabase execSQL err:' + err);
      } else {
        console.info('StandardDatabase execSQL done. sql:' + sql + ",selectionArgs:" + bindArgs);
      }
    });
  }

  // @ts-ignore
  compileStatement(sql: dataRdb.RdbPredicates, tableName?: string): DatabaseStatement {
    if (tableName != undefined) {
      // @ts-ignore
      let sQLiteStatement=new  SQLiteStatement(sql, <dataRdb.RdbStore> this.delegate, tableName);
      sQLiteStatement.setStandardDatabase(this);

      return new StandardDatabaseStatement(sQLiteStatement);
    }
    // @ts-ignore
    let sQLiteStatement=  new SQLiteStatement(sql, <dataRdb.RdbStore> this.delegate);
    sQLiteStatement.setStandardDatabase(this);
    return new StandardDatabaseStatement(sQLiteStatement);
  }

  isDbLockedByCurrentThread(): boolean {
    return false;
  }

  isOpen(): boolean {
    return false;
  }

  close() {
  }

  getRawDatabase(): any {
    return this.delegate;
  }

  //@ts-ignore
  getSQLiteDatabase(): dataRdb.RdbStore {
    return this.delegate;
  }

  addTableChangedListener(onTableChangedListener: OnTableChangedListener<any>): void {
    this.myOnTableChangedListener = onTableChangedListener

  }

  getTableChangedListener(): OnTableChangedListener<any>{
    return this.myOnTableChangedListener
  }
  removeTableChangedListener(){
    this.myOnTableChangedListener=null

  }
}
