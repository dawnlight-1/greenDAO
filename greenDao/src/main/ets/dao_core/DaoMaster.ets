
import {AbstractDaoMaster} from '../dao_core/AbstractDaoMaster';
import {StandardDatabase} from '../dao_core/database/StandardDatabase';
import {Database} from '../dao_core/database/Database';
import {DatabaseOpenHelper} from '../dao_core/database/DatabaseOpenHelper';
import {IdentityScopeType} from '../dao_core/identityscope/IdentityScopeType';
import {BaseDao} from './BaseDao';
import {DaoSession} from './DaoSession';
import dataRdb from '@ohos.data.rdb'

var schema_version: number = 1;
// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/**
 * Master of DAO (schema version 1000): knows all DAOs.
 */
export class DaoMaster extends AbstractDaoMaster {

    /** Creates underlying database table using DAOs. */
    public static createAllTables(db: Database, ifNotExists: boolean): void {
        let entitys = globalThis.exports.default.data['entitys'];
        if(entitys){
          for(let entity of entitys){
            BaseDao.createTable(db, ifNotExists, entity);
          }
        }
    }

    /** Drops underlying database table using DAOs. */
    public static dropAllTables(db: Database, ifExists: boolean): void {

      let entitys = globalThis.exports.default.data['entitys'];
      if(entitys){
        for(let entity of entitys){
          BaseDao.dropTable(db, ifExists, entity);
        }
      }
    }

    /**
     * WARNING: Drops all table on Upgrade! Use only during development.
     * Convenience method using a {@link DevOpenHelper}.
     */
    public static async newDevSession(context: any/*Context*/, name: string): Promise<DaoSession> {
        let db: Database = await new DevOpenHelper(context, name).getWritableDb();
        let daoMaster: DaoMaster = new DaoMaster(db);
        let daoSession: DaoSession = daoMaster.newSession()
        return daoSession;
    }

    //@ts-ignore
    constructor(db: any /*dataRdb.RdbStore | Database*/){

      let cls = db.constructor.name;
      if(cls == 'StandardDatabase'){
        super(db, schema_version);
      }else{
        //@ts-ignore
        super(new StandardDatabase(db), schema_version);
      }
      let entitys = globalThis.exports.default.data['entitys'];
      if(entitys){
        for(let entity of entitys){
          this.registerDaoClass(entity);
        }
      }
    }

  public newSession(type=IdentityScopeType.Session): DaoSession {
    return new DaoSession(this.db, type, this.daoConfigMap);
  }
}


    /**
     * Calls {@link #createAllTables(Database, boolean)} in {@link #onCreate(Database)} -
     */
    export abstract class OpenHelper extends DatabaseOpenHelper {

        constructor(context: any, name: string, factory?: any){
           super(context, name, schema_version, factory);
        }

        public onCreate_D(db: Database): void {
          DaoMaster.createAllTables(db, true);
        }
    }

    /** WARNING: Drops all table on Upgrade! Use only during development. */
    export class DevOpenHelper extends OpenHelper {

      constructor(context: any, name: string , factory?: object){
         super(context, name, factory);

      }

      public onUpgrade_D(db: Database, oldVersion: number, newVersion: number): void {
        let migrations= this.getMigration();
        migrations.forEach(migrations_item => {
          migrations_item.execute()
        });
      }
    }
